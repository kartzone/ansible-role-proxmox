Role Name
=========

This role is used for the post-installation of a Proxmox VE node on Debian

License
-------

BSD

Author Information
------------------

https://www.kartzone.fr
